﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class obstacleManager : MonoBehaviour {

    public int strength;
    public Color[] obsColor;
    public GameObject zero;
    public GameObject one;
    public GameObject two;
    public GameObject obstacleMesh;

    public bool gotHit;

    Rigidbody rb;

    public GameObject gameController;

    // Use this for initialization
    void Start () {
        rb = this.GetComponent<Rigidbody>();
        determineNumber();
        gameController = GameObject.FindGameObjectWithTag("GameController");

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void determineNumber()
    {
        HideAllNum();
        obstacleMesh.GetComponent<Renderer>().material.color = obsColor[strength];
        if (strength == 2)
        {
            two.SetActive(true);
        }
        else if (strength == 1)
        {
            one.SetActive(true);
        }
        else if (strength == 0)
        {
            zero.SetActive(true);
        }
    }

    void HideAllNum()
    {
        two.SetActive(false);
        one.SetActive(false);
        zero.SetActive(false);
    }

    public void RuinStrength()
    {
        if(strength > 0 && gotHit == false)
        {
            strength -= 1;
            determineNumber();
            gotHit = true;
            gameController.GetComponent<gameController>().points += 1;
            //gameController.GetComponent<gameController>().movesLeft -= 1;
            gameController.GetComponent<gameController>().UpdateUIText();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player" )
        {
            gotHit = false;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            //gotHit = true;
        }
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.gameObject.tag == "Player")
    //    {
    //        gotHit = false;
    //    }
    //}
}
