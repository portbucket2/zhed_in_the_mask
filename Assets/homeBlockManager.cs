﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class homeBlockManager : MonoBehaviour {

    public GameObject gameController;
    public GameObject homeDead;
    public GameObject homeAlive;

    // Use this for initialization
    void Start () {
        gameController = GameObject.FindGameObjectWithTag("GameController");
    }
	
	// Update is called once per frame
	void Update () {
		if(gameController.GetComponent<gameController>().pointNeeded <= gameController.GetComponent<gameController>().points)
        {
            homeAlive.SetActive(true);
            homeDead.SetActive(false);
        }
        else
        {
            homeAlive.SetActive(false);
            homeDead.SetActive(true);
        }
	}
}
