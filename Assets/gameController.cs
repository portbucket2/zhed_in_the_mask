﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameController : MonoBehaviour {
    public Vector2 mousePos;
    public Vector2 oldMousePos;
    public Vector2 newMousePos;
    public Vector2 swipeTravel;

    public Camera swipeCam;

    public float minSwipeDistance;

    public bool swipeUp;
    public bool swipeDown;
    public bool swipeRight;
    public bool swipeLeft;

    public bool swiping;

    public GameObject player;
    public float playerMoveSpeed;
    public Vector3 obstaclePos;
    public Vector3 borderBlockPos;
    public Vector3 playerDesiredPos;

    public bool upMode;
    public bool downMode;
    public bool rightMode;
    public bool leftMode;

    

    public bool upBlocked;
    public bool downBlocked;
    public bool rightBlocked;
    public bool leftBlocked;

    public RaycastHit hit;

    public LayerMask obstacleLayer;


    public int level;
    public int maxLevel;
    public int points;
    public int movesLeft;
    public Text pointsText;
    public Text levelText;
    public Text movesLeftText;

    public int pointNeeded;

    public bool isMooving;
    public bool playerMoveNoted;
    public int playerMoves;

    public GameObject gameOverPanel;
    public GameObject levelClearPanel;
    public GameObject allCompletePanel;

    public GameObject homeBlock;
    public GameObject levelManager;
    

    //public Vector3 playerPosBeforeMoving;
    //public Vector3 playerPosAfterMoving;
    //public bool isPlayerMoved;

    // Use this for initialization
    void Start () {

        level = PlayerPrefs.GetInt("level", level);
        
        UpdateUIText();
        //playerPosBeforeMoving = player.transform.position;
        playerDesiredPos = player.transform.position;
        gameOverPanel.SetActive(false);
        allCompletePanel.SetActive(false);

        //homeBlock = GameObject.FindGameObjectWithTag("homeBlock");
        levelManager = GameObject.FindGameObjectWithTag("levelManager");

    }
	
	// Update is called once per frame
	void Update () {
        PlayerMoveSensing();
        if(movesLeft > 0)
        {
            GetSwipe();
        }
        else if(movesLeft <= 0)
        {
            Invoke( "GameOver", 1f);
        }
        //GetSwipe();
        MovePlayer();
        senseLevelClearance();
        //GetHitData();
        //RaycastingRight();
        DetermineHomeBlock();
        UpdateUIText();

    }
    void RaycastingRight()
    {
        //RaycastHit hit;
        if (Physics.Raycast(player.transform.position, transform.right, out hit, 0.6f, obstacleLayer))
        {
            //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            GetHitData();
            ResetLimitations();
            rightBlocked = true;
            obstaclePos = hit.transform.position;
            playerDesiredPos = new Vector3((obstaclePos.x - 1), player.transform.position.y, player.transform.position.z);
            SensePlayerDesiredPosHomeBlock();
            ResetModes();
            //Debug.Log("Did Hit");
        }
    }
    void RaycastingLeft()
    {
        //RaycastHit hit;
        if (Physics.Raycast(player.transform.position, -transform.right, out hit, 0.6f, obstacleLayer))
        {
            //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            GetHitData();
            ResetLimitations();
            leftBlocked = true;
            obstaclePos = hit.transform.position;
            playerDesiredPos = new Vector3((obstaclePos.x + 1), player.transform.position.y, player.transform.position.z);
            SensePlayerDesiredPosHomeBlock();
            ResetModes();
            //Debug.Log("Did Hit");
        }
    }
    void RaycastingUp()
    {
        //RaycastHit hit;
        if (Physics.Raycast(player.transform.position, transform.forward, out hit, 0.6f, obstacleLayer))
        {
            //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            GetHitData();
            ResetLimitations();
            upBlocked = true;
            obstaclePos = hit.transform.position;
            playerDesiredPos = new Vector3(player.transform.position.x, player.transform.position.y, (obstaclePos.z - 1));
            SensePlayerDesiredPosHomeBlock();
            ResetModes();
            //Debug.Log("Did Hit");
        }
    }
    void RaycastingDown()
    {
        //RaycastHit hit;
        if (Physics.Raycast(player.transform.position, -transform.forward, out hit, 0.6f, obstacleLayer))
        {
            //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            GetHitData();
            ResetLimitations();
            downBlocked = true;
            obstaclePos = hit.transform.position;
            playerDesiredPos = new Vector3(player.transform.position.x, player.transform.position.y, (obstaclePos.z + 1));
            SensePlayerDesiredPosHomeBlock();
            ResetModes();
            //Debug.Log("Did Hit");
        }
    }

    void GetHitData()
    {
        if(hit.transform.gameObject.tag == "obstacle")
        {
            hit.transform.GetComponent<obstacleManager>().RuinStrength();
            //Debug.Log("GetiingHitData");
        }
    }
    void GetSwipe()
    {
        //mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //mousePos = swipeCam.ScreenToWorldPoint(Input.mousePosition);
        mousePos = Input.mousePosition / Screen.width;
        if (Input.GetMouseButtonDown(0) && isMooving == false)
        {
            oldMousePos = newMousePos = mousePos;
            //Raycasting();

        }
        if (Input.GetMouseButton(0) && swiping == false)
        {
            newMousePos = mousePos;
            swipeTravel = newMousePos - oldMousePos;
            if (Mathf.Abs(swipeTravel.x) > Mathf.Abs(swipeTravel.y) && Mathf.Abs(swipeTravel.x) > minSwipeDistance )
            {
                if (swipeTravel.x > 0)
                {
                    RaycastingRight();
                    swipeRight = true;
                    swiping = true;

                    upMode    = false ;
                    downMode  = false ;
                    rightMode = true ;
                    leftMode  = false ;
                    //hit.transform.position += new Vector3(1, 0, 0);
                    //targetBlockPos =  hit.transform.position + new Vector3(1, 0, 0);
                    //if (hit.transform.GetComponent<blockMover>().rightBooked == false)
                    //{
                    //    hit.transform.GetComponent<blockMover>().targetBlockPos += new Vector3(1, 0, 0);
                    //}



                }
                else
                {
                    RaycastingLeft();
                    swipeLeft = true;
                    swiping = true;

                    upMode = false;
                    downMode = false;
                    rightMode = false;
                    leftMode = true;
                    //hit.transform.position += new Vector3(-1, 0, 0);
                    //targetBlockPos = hit.transform.position + new Vector3(-1, 0, 0);
                    //hit.transform.GetComponent<blockMover>().targetBlockPos += new Vector3(-1, 0, 0);
                    //if (hit.transform.GetComponent<blockMover>().leftBooked == false)
                    //{
                    //    hit.transform.GetComponent<blockMover>().targetBlockPos += new Vector3(-1, 0, 0);
                    //}
                }

            }
            else if (Mathf.Abs(swipeTravel.y) > Mathf.Abs(swipeTravel.x) && Mathf.Abs(swipeTravel.y) > minSwipeDistance)
            {
                if (swipeTravel.y > 0)
                {
                    RaycastingUp();
                    swipeUp = true;
                    swiping = true;

                    upMode = true;
                    downMode = false;
                    rightMode = false;
                    leftMode = false;
                    //hit.transform.position += new Vector3(0, 1, 0);
                    //targetBlockPos = hit.transform.position + new Vector3(0, 1, 0);
                    //hit.transform.GetComponent<blockMover>().targetBlockPos += new Vector3(0, 1, 0);
                    //if (hit.transform.GetComponent<blockMover>().upBooked == false)
                    //{
                    //    hit.transform.GetComponent<blockMover>().targetBlockPos += new Vector3(0, 1, 0);
                    //}
                }
                else
                {
                    RaycastingDown();
                    swipeDown = true;
                    swiping = true;

                    upMode = false;
                    downMode = true;
                    rightMode = false;
                    leftMode = false;
                    //hit.transform.position += new Vector3(0, -1, 0);
                    //targetBlockPos = hit.transform.position + new Vector3(0, -1, 0);
                    //hit.transform.GetComponent<blockMover>().targetBlockPos += new Vector3(0, -1, 0);
                    //if (hit.transform.GetComponent<blockMover>().downBooked == false)
                    //{
                    //    hit.transform.GetComponent<blockMover>().targetBlockPos += new Vector3(0, -1, 0);
                    //}
                }

            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            ResetSwipes();
            swipeTravel = new Vector2(0, 0);
            swiping = false;
        }
    }

    void SensePlayerDesiredPosHomeBlock()
    {
        if (hit.transform.gameObject.tag == "homeAlive")
        {

            playerDesiredPos = hit.transform.position;
        }
    }

    void MovePlayer()
    {
        if(upMode == true && upBlocked == false)
        {
            player.transform.Translate(0, 0, playerMoveSpeed * Time.deltaTime * 50f);
            RaycastingUp();
        }
        else if(downMode == true && downBlocked == false)
        {
            player.transform.Translate(0, 0, -playerMoveSpeed * Time.deltaTime * 50f);
            RaycastingDown();
        }
        else if (rightMode == true && rightBlocked == false)
        {
            player.transform.Translate(playerMoveSpeed * Time.deltaTime * 50f, 0,0);
            RaycastingRight();
            //playerPosAfterMoving = player.transform.position;
        }
        else if (leftMode == true && leftBlocked == false)
        {
            player.transform.Translate(-playerMoveSpeed * Time.deltaTime * 50f, 0, 0);
            RaycastingLeft();
        }
        //isMooving = true;
    }

    void ResetSwipes()
    {
        swipeUp = false;
        swipeDown = false;
        swipeRight = false;
        swipeLeft = false;
    }
    void ResetModes()
    {
        upMode = false;
        downMode = false;
        rightMode = false;
        leftMode = false;
        player.transform.position = playerDesiredPos;
    }

    void ResetLimitations()
    {
        upBlocked=false;
        downBlocked= false;
        rightBlocked = false;
        leftBlocked= false;
        
    }
    public void UpdateUIText()
    {
        levelText.text = "" + level;
        pointsText.text = "" + points+ "/" + pointNeeded ;
        movesLeftText.text = "" + movesLeft;
    }

    public void PlayerMoveSensing()
    {
        if(Vector3.Distance(player.transform.position, playerDesiredPos) > 0.2f)
        {
            isMooving = true;
            if(playerMoveNoted == false)
            {
                //movesLeft -= 1;
                levelManager.GetComponent<levelManager>().activeLevel.GetComponent<levelStrengthPoints>().movesLeft -= 1;
                playerMoveNoted = true;
                movesLeftText.text = "" + movesLeft;
            }
            Debug.Log("Moved");
        }
        else
        {
            isMooving = false;
            playerMoveNoted = false;
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(1);
    }
    public void BackToMenu()
    {
        SceneManager.LoadScene(0);
    }
    public void GameOver()
    {
        gameOverPanel.SetActive(true);
    }
    public void LevelClear()
    {
        levelClearPanel.SetActive(true);
    }

    //public void senseLevelClearance()
    //{
    //    if(points >= pointNeeded && Vector3.Distance(homeBlock.transform.position,playerDesiredPos) < 0.1f)
    //    {
    //        Invoke("LevelClear", 0.5f); 
    //    }
    //}
    public void senseLevelClearance()
    {
        pointNeeded = levelManager.GetComponent<levelManager>().activeLevel.GetComponent<levelStrengthPoints>().pointsNeeded;
        movesLeft = levelManager.GetComponent<levelManager>().activeLevel.GetComponent<levelStrengthPoints>().movesLeft;
        if (points >= pointNeeded && Vector3.Distance(homeBlock.transform.position, player.transform.position) < 0.5f)
        {
            playerDesiredPos = homeBlock.transform.position;
            Invoke("LevelClear", 0.5f);
        }
    }

    void DetermineHomeBlock()
    {
        homeBlock = levelManager.GetComponent<levelManager>().activeLevel. transform.Find("homeBlock").gameObject;
    }

    public void UpdateNextLevel()
    {
        if(level < maxLevel)
        {
            level += 1;
            PlayerPrefs.SetInt("level", level);
            SceneManager.LoadScene(1);
        }
        else if(level >= maxLevel)
        {
            allCompletePanel.SetActive(true);
        }
        
    }
    
}
