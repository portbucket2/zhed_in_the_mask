﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelManager : MonoBehaviour {

    public int level;
    public int levelId;

    public GameObject[] levelGrounds;
    public GameObject activeLevel;

	// Use this for initialization
	void Start () {

        level = PlayerPrefs.GetInt("level", level);
        EnableRightLevelGroud();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void EnableRightLevelGroud()
    {
        levelId = level - 1;
        for (int i = 0; i < levelGrounds.Length; i++)
        {
            if(i == levelId)
            {
                levelGrounds[i].SetActive(true);
                activeLevel = levelGrounds[i];
            }
            else
            {
                levelGrounds[i].SetActive(false);
            }
        }
    }
}
