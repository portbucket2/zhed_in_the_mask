﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class menuManager : MonoBehaviour {

    public int level;
    public int levelUnlocked;
    public Text levelText;
    public GameObject levelsPanel;
    

	// Use this for initialization
	void Start () {
        

        level = PlayerPrefs.GetInt("level", level);
        levelUnlocked = PlayerPrefs.GetInt("levelUnlocked", levelUnlocked);
        if (level == 0)
        {
            level = 1;
        }
        PlayerPrefs.SetInt("level", level);
        UpdateUiTexts();
        levelsPanel.SetActive(false);

    }
	
	// Update is called once per frame
	void Update () {
        
        if (Input.GetKeyDown(KeyCode.Space))
        {
            UpdateLevel();

        }
        UpdateLevelUnlocked();
        
        if (Input.GetKeyDown(KeyCode.D))
        {
            degradeLevel();
        }

    }

    public void UpdateLevel()
    {
        level += 1;
        PlayerPrefs.SetInt("level", level);
        levelText.text = "" + level;
        
    }
    public void UpdateLevelUnlocked()
    {
        if (level > levelUnlocked)
        {
            levelUnlocked = level;
            PlayerPrefs.SetInt("levelUnlocked", levelUnlocked);

        }
    }
    public void degradeLevel()
    {
        level -= 1;
        PlayerPrefs.SetInt("level", level);
        levelText.text = "" + level;

    }
    public void ResetLevel()
    {
        level = 1;
        levelUnlocked = 1;
        PlayerPrefs.SetInt("level", level);
        PlayerPrefs.SetInt("levelUnlocked", levelUnlocked);
        levelText.text = "" + level;

    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    void UpdateUiTexts()
    {
        levelText.text = "" + level;
    }

    public void GetMeLevelsPanel()
    {
        levelsPanel.SetActive(true);
    }
    public void ExitLevelsPanel()
    {
        levelsPanel.SetActive(false);
    }
}
