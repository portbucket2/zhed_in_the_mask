﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class levelIndividualManager : MonoBehaviour {

    
    public int levelInButton;
    public GameObject levelNumberText;
    public GameObject lockedImage;
    public GameObject levelButton;

    public int level;
    public int levelUnlocked;

    // Use this for initialization
    void Start () {

        lockedImage = this.transform.Find("lockedImage").gameObject;
        levelButton = this.transform.Find("LevelNumberButton").gameObject;
        levelNumberText = levelButton.transform.Find("LevelValue").gameObject;

        levelNumberText.GetComponent<Text>().text = "" + levelInButton;
        level = PlayerPrefs.GetInt("level", level);
        levelUnlocked = PlayerPrefs.GetInt("levelUnlocked", levelUnlocked);

    }
	
	// Update is called once per frame
	void Update () {
        checkUnlocking();
        level = PlayerPrefs.GetInt("level", level);
        levelUnlocked = PlayerPrefs.GetInt("levelUnlocked", levelUnlocked);

    }

    public void checkUnlocking()
    {
        if(levelInButton <= levelUnlocked)
        {
            levelButton.SetActive(true);
            lockedImage.SetActive(false);
        }
        else
        {
            levelButton.SetActive(false);
            lockedImage.SetActive(true);
        }
        levelNumberText.GetComponent<Text>().text = "" + levelInButton;
    }

    public void StartLevel()
    {
        level = levelInButton;
        PlayerPrefs.SetInt("level", level);
        SceneManager.LoadScene(1);
    }

    
}
